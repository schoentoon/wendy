package main

import (
	log "github.com/sirupsen/logrus"
	"os"
	"time"

	"gopkg.in/yaml.v2"
)

// Config structure of the config file
type Config struct {
	DB     string `yaml:"db"`
	Listen string `yaml:"listen"`
	Cron   struct {
		Dep time.Duration `yaml:"dep"`
	} `yaml:"cron"`
	LogLevel log.Level `yaml:"loglevel"`
}

// ReadConfig reads a file into the config structure
func ReadConfig(file string) (*Config, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}

	out := &Config{}
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&out)
	if err != nil {
		return nil, err
	}

	return out, err
}
