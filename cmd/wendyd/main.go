package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sync"

	log "github.com/sirupsen/logrus"
	"gitlab.com/schoentoon/wendy/pkg/database"
	"gitlab.com/schoentoon/wendy/pkg/handlers"
	"gitlab.com/schoentoon/wendy/pkg/workers"

	_ "github.com/mattn/go-sqlite3"
)

func run(cancel <-chan os.Signal, cfg *Config, db *sql.DB) {
	var wg sync.WaitGroup
	wg.Add(3)
	server := http.Server{
		Addr: cfg.Listen,
	}
	go func() {
		defer wg.Done()
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			log.Errorf("HTTP Server failed with %s", err)
		}
	}()

	baseCheck := workers.NewBaseChecker(cfg.Cron.Dep)
	go baseCheck.Run(db, &wg)

	webhookProcessor := workers.NewWebhookProcessor(db)
	go webhookProcessor.Run(&wg)
	baseCheck.RegisterUpdater(webhookProcessor)

	for {
		select {
		case <-cancel:
			log.Infof("Received SIGTERM, closing cleanly")
			baseCheck.Close()
			webhookProcessor.Close()
			err := server.Shutdown(context.Background())
			if err != nil {
				log.Errorf("Failed shutting down the http server properly: %s", err)
			}
			wg.Wait()
			return
		case <-baseCheck.C:
			err := baseCheck.Dispatch(db)
			if err != nil {
				log.Errorf("DepCheckDispatched returned %s", err)
			}
		}
	}
}

func main() {
	var cfgfile = flag.String("config", "config.yml", "Config file location")
	flag.Parse()

	cfg, err := ReadConfig(*cfgfile)
	if err != nil {
		log.Fatal(err)
	}

	log.SetLevel(cfg.LogLevel)
	db, err := sql.Open("sqlite3", fmt.Sprintf("file:%s?cache=shared", cfg.DB))
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	migrator := database.NewWendyMigrator()
	err = migrator.Migrate(db)
	if err != nil {
		log.Fatal(err)
	}

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	http.Handle("/api/new/dockerfile", &handlers.AddDockerfile{DB: db})
	http.Handle("/api/list/jobs", &handlers.ListJobs{DB: db})

	run(c, cfg, db)
}
