package main

import (
	"flag"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/briandowns/spinner"
	"github.com/c-bata/go-prompt"
	"github.com/c-bata/go-prompt/completer"
	log "github.com/sirupsen/logrus"
)

type Command interface {
	Name() string
	WantSpinner() bool
	Description() string
	Autocomplete(host string, in prompt.Document) []prompt.Suggest
	Execute(host, args string, out io.Writer) error
}

var Host string
var Commands = []Command{
	&AddDockerfile{},
	&ListJobs{},
}

func completerFunc(in prompt.Document) []prompt.Suggest {
	w := in.GetWordBeforeCursor()
	line := in.TextBeforeCursor()
	blocks := strings.SplitN(line, " ", 2)
	cmd := blocks[0]

	for _, c := range Commands {
		if cmd == c.Name() {
			return c.Autocomplete(Host, in)
		}
	}

	if len(blocks) != 1 || line == "" {
		return []prompt.Suggest{}
	}

	out := make([]prompt.Suggest, len(Commands))
	for i, c := range Commands {
		out[i] = prompt.Suggest{Text: c.Name(), Description: c.Description()}
	}
	return prompt.FilterHasPrefix(out, w, true)
}

type StopSpinnerWriter struct {
	Out     io.Writer // this is the underlying writer
	Spinner *spinner.Spinner
}

func (w *StopSpinnerWriter) Write(p []byte) (int, error) {
	w.CloseSpinner()
	return w.Out.Write(p)
}

func (w *StopSpinnerWriter) CloseSpinner() {
	if w.Spinner != nil {
		w.Spinner.Stop()
		w.Spinner = nil
	}
}

func executorFunc(in string) {
	in = strings.TrimSpace(in)

	blocks := strings.SplitN(in, " ", 2)
	if blocks[0] == "" {
		return
	}

	var cmd Command
	for _, c := range Commands {
		if c.Name() == blocks[0] {
			cmd = c
			break
		}
	}

	if cmd == nil {
		fmt.Println("Invalid command")
		return
	}

	out := &StopSpinnerWriter{Out: os.Stdout}
	if cmd.WantSpinner() {
		s := spinner.New(spinner.CharSets[11], 100*time.Millisecond)
		s.HideCursor = true
		s.Start()
		out.Spinner = s
		defer out.CloseSpinner()
	}

	var err error
	if len(blocks) == 1 {
		err = cmd.Execute(Host, "", out)
	} else {
		err = cmd.Execute(Host, blocks[1], out)
	}
	if err == flag.ErrHelp {
		return
	}
	if err != nil {
		fmt.Printf("Error: %s\n", err)
	}
}

func main() {
	if len(os.Args) == 1 {
		log.Fatalf("Needs at least 1 argument")
	}
	uri, err := url.Parse(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	Host = uri.Host

	p := prompt.New(
		executorFunc,
		completerFunc,
		prompt.OptionCompletionWordSeparator(completer.FilePathCompletionSeparator),
	)

	p.Run()
}
