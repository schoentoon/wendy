package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/c-bata/go-prompt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/schoentoon/wendy/pkg/handlers"
)

type AddDockerfile struct {
}

func (b *AddDockerfile) Name() string {
	return "add"
}

func (b *AddDockerfile) Description() string {
	return "Add a new dockerfile into the system"
}

func (b *AddDockerfile) Autocomplete(host string, in prompt.Document) []prompt.Suggest {
	path := in.GetWordBeforeCursor()
	if path == "" || strings.HasPrefix(path, "-") {
		return []prompt.Suggest{
			prompt.Suggest{Text: "--name", Description: "The name to use for this job"},
		}
	}

	dir, base, err := cleanFilePath(path)
	if err != nil {
		log.Fatal("completer: cannot get current user:" + err.Error())
		return nil
	}

	files, err := ioutil.ReadDir(dir)
	if err != nil && os.IsNotExist(err) {
		return nil
	} else if err != nil {
		log.Fatal("completer: cannot read directory items:" + err.Error())
		return nil
	}

	suggests := make([]prompt.Suggest, 0, len(files))
	for _, f := range files {
		suggests = append(suggests, prompt.Suggest{Text: f.Name()})
	}
	return prompt.FilterHasPrefix(suggests, base, false)

}

func (b *AddDockerfile) WantSpinner() bool {
	return true
}

func (b *AddDockerfile) Execute(host, argv string, out io.Writer) error {
	flagSet := flag.NewFlagSet("name", flag.ContinueOnError)
	name := flagSet.String("name", "", "The name to use for this job")
	err := flagSet.Parse(strings.Split(argv, " "))
	if err != nil {
		return err
	}

	if *name == "" {
		return fmt.Errorf("We need a name for this new job")
	} else if len(flagSet.Args()) == 0 {
		return fmt.Errorf("We need a Dockerfile to actually add")
	}

	dockerfile, err := ioutil.ReadFile(flagSet.Arg(0))
	if err != nil {
		return err
	}

	request := handlers.AddDockerfileRequest{
		Name:       *name,
		Dockerfile: string(dockerfile),
	}
	buffer := &bytes.Buffer{}
	err = json.NewEncoder(buffer).Encode(request)
	if err != nil {
		return err
	}

	uri := url.URL{
		Scheme: "http",
		Host:   host,
		Path:   "/api/new/dockerfile",
	}
	res, err := http.Post(uri.String(), "application/json", buffer)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	_, err = io.Copy(out, res.Body)
	return err
}
