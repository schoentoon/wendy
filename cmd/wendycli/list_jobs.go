package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"

	"github.com/c-bata/go-prompt"
	"gitlab.com/schoentoon/wendy/pkg/handlers"
)

type ListJobs struct {
}

func (b *ListJobs) Name() string {
	return "list-jobs"
}

func (b *ListJobs) Description() string {
	return "Lists all the jobs known by wendyd"
}

func (b *ListJobs) Autocomplete(host string, in prompt.Document) []prompt.Suggest {
	return nil

}

func (b *ListJobs) WantSpinner() bool {
	return true
}

func (b *ListJobs) Execute(host, argv string, out io.Writer) error {
	uri := url.URL{
		Scheme: "http",
		Host:   host,
		Path:   "/api/list/jobs",
	}
	res, err := http.Get(uri.String())
	if err != nil {
		return err
	}
	defer res.Body.Close()

	decoder := json.NewDecoder(res.Body)
	for {
		var entry handlers.ListJobsEntry
		err = decoder.Decode(&entry)
		if err == io.EOF {
			return nil
		} else if err != nil {
			return err
		}
		fmt.Fprintf(out, "%s\n", entry.Name)
	}
}
