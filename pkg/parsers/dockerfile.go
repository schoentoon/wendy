package parsers

import (
	"bytes"

	"github.com/docker/distribution/reference"
	"github.com/moby/buildkit/frontend/dockerfile/parser"
)

type Dockerfile struct {
	BaseImages []string
}

func ParseDockerfile(content string) (*Dockerfile, error) {
	res, err := parser.Parse(bytes.NewBufferString(content))
	if err != nil {
		return nil, err
	}

	out := &Dockerfile{
		BaseImages: []string{},
	}

	for _, child := range res.AST.Children {
		switch child.Value {
		case "from":
			// scratch isn't a real image we can actually monitor
			if child.Next.Value == "scratch" {
				continue
			}

			name, err := reference.ParseNormalizedNamed(child.Next.Value)
			if err != nil {
				return nil, err
			}
			name = reference.TagNameOnly(name)
			image := name.String()
			for _, img := range out.BaseImages {
				// we if already are in BaseImages we just bail out
				if img == image {
					continue
				}
			}
			out.BaseImages = append(out.BaseImages, image)
		}
	}

	return out, nil
}
