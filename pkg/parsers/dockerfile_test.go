package parsers

import "testing"

import "github.com/stretchr/testify/assert"

func TestParseDockerfile(t *testing.T) {
	out, err := ParseDockerfile("FROM alpine")
	if assert.Nil(t, err) {
		assert.NotZero(t, len(out.BaseImages))
		assert.Equal(t, "docker.io/library/alpine:latest", out.BaseImages[0])
	}

	out, err = ParseDockerfile("FROM scratch")
	if assert.Nil(t, err) {
		assert.Zero(t, len(out.BaseImages))
	}
}
