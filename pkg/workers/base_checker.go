package workers

import (
	"database/sql"
	"encoding/json"
	"sync"
	"time"

	"github.com/docker/distribution/reference"
	log "github.com/sirupsen/logrus"

	"gitlab.com/schoentoon/wendy/pkg/images"
)

type BaseUpdated interface {
	BaseImageUpdated(name string, depID int64) error
}

type basejob struct {
	Name  reference.Named
	DepID int64
}

func (b *basejob) String() string {
	return b.Name.String()
}

type BaseChecker struct {
	*time.Ticker
	channel chan basejob // This channel will simply receive jobs it should process

	Downloader *images.DownloadManager
	Updaters   []BaseUpdated
}

func NewBaseChecker(duration time.Duration) *BaseChecker {
	return &BaseChecker{
		Ticker:     time.NewTicker(duration),
		channel:    make(chan basejob, 64),
		Downloader: images.NewDownloadManager(),
		Updaters:   []BaseUpdated{},
	}
}

func (b *BaseChecker) RegisterUpdater(updater BaseUpdated) {
	b.Updaters = append(b.Updaters, updater)
}

func (b *BaseChecker) Close() {
	close(b.channel)
	b.Ticker.Stop()
}

func (b *BaseChecker) Run(db *sql.DB, wg *sync.WaitGroup) {
	defer wg.Done()
	for job := range b.channel {
		log.Infof("Received dependency: %s", job.Name)
		err := b.depCheck(db, job)
		if err != nil {
			log.WithField("image", job).Errorf("depCheck returned %s", err)
		}
	}
}

func (b *BaseChecker) Dispatch(db *sql.DB) error {
	rows, err := db.Query(`SELECT id, name FROM deps`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var id int64
		var name string
		err = rows.Scan(&id, &name)
		if err != nil {
			return err
		}
		parsed, err := reference.ParseNormalizedNamed(name)
		if err != nil {
			return err
		}
		job := basejob{
			Name:  parsed,
			DepID: id,
		}
		b.channel <- job
	}
	return nil
}

func (b *BaseChecker) depCheck(db *sql.DB, j basejob) error {
	clog := log.WithField("image", j)

	tx, err := db.Begin()
	if err != nil {
		return err
	}
	defer func() {
		err := tx.Rollback()
		if err != nil && err != sql.ErrTxDone {
			clog.Errorf("An error occurred while rolling back a transaction: %s", err)
		}
	}()

	var oldSerialized []byte
	row := tx.QueryRow(`SELECT manifest FROM deps WHERE id = ?`, j.DepID)
	err = row.Scan(&oldSerialized)
	if err != nil {
		return err
	}

	var oldManifest images.Manifest
	if oldSerialized != nil {
		err := json.Unmarshal(oldSerialized, &oldManifest)
		if err != nil {
			clog.Warnf("We seem to have encountered an invalid stored manifest: %s", err)
		}
		oldManifest.Named, err = reference.ParseNormalizedNamed(oldManifest.Name)
		if err != nil {
			clog.Warnf("We seem to have encountered an invalid stored manifest: %s", err)
		}
	}

	manifest, err := b.Downloader.Manifest(j.Name)
	if err != nil {
		clog.Errorf("Error retrieving manifest: %s", err)
		return err
	} else if oldSerialized == nil || manifest.Changed(&oldManifest) {
		for _, updater := range b.Updaters {
			err = updater.BaseImageUpdated(j.Name.Name(), j.DepID)
			if err != nil {
				clog.Warnf("Something went wrong while dispatching the base image update: %s", err)
			}
		}
	}
	marshalled, err := json.Marshal(manifest)
	if err != nil {
		return err
	}
	_, err = tx.Exec(`UPDATE deps SET manifest = ? WHERE id = ?`, string(marshalled), j.DepID)
	if err != nil {
		return err
	}
	return tx.Commit()
}
