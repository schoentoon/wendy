package workers

import (
	"database/sql"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/schoentoon/wendy/pkg/database"
)

func TestBaseCheckDispatcher(t *testing.T) {
	db, cancel := database.TmpSqliteDB(t, true, func(db *sql.DB) error {
		_, err := db.Exec(`INSERT INTO deps(name) VALUES("docker.io/library/alpine:latest");`)
		return err
	})
	defer cancel()

	base := NewBaseChecker(time.Minute)
	defer base.Close()

	err := base.Dispatch(db)
	if assert.Nil(t, err) {
		select {
		case m := <-base.channel:
			assert.Equal(t, int64(1), m.DepID)
			assert.Equal(t, "docker.io/library/alpine:latest", m.Name.String())
		default:
			t.Error("We didn't get any messages on the base check channel somehow")
		}
	}
}
