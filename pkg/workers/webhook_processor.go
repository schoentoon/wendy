package workers

import (
	"database/sql"
	"net/http"
	"sync"

	log "github.com/sirupsen/logrus"
)

type WebhookProcessor struct {
	DB      *sql.DB
	Channel chan int64 // This channel will receive the IDs of jobs that should be executed
}

func NewWebhookProcessor(db *sql.DB) *WebhookProcessor {
	return &WebhookProcessor{
		DB:      db,
		Channel: make(chan int64, 64),
	}
}

func (w *WebhookProcessor) Close() {
	w.Channel <- -1
}

func (w *WebhookProcessor) BaseImageUpdated(name string, depID int64) error {
	rows, err := w.DB.Query(`SELECT job_id FROM job_deps_link WHERE dep_id = ?`, depID)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var jobID int64
		err = rows.Scan(&jobID)
		if err != nil {
			return err
		}
		w.Channel <- jobID
	}
	return nil
}

func (w *WebhookProcessor) Run(wg *sync.WaitGroup) {
	defer wg.Done()
	for jobID := range w.Channel {
		if jobID == -1 {
			close(w.Channel)
			return
		}
		err := w.executeWebhooks(jobID)
		if err != nil {
			log.WithField("job", jobID).Errorf("Error while selecting webhooks: %s", err)
		}
	}
}

func (w *WebhookProcessor) executeWebhooks(jobID int64) error {
	rows, err := w.DB.Query(`SELECT webhook FROM webhooks WHERE job_id = ?`, jobID)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		var webhook string
		err = rows.Scan(&webhook)
		if err != nil {
			return err
		}

		err = w.callWebhook(jobID, webhook)
		if err != nil {
			log.WithField("job", jobID).Errorf("Error occurred while processing webhook %s: %s", webhook, err)
		}
	}

	return nil
}

func (w *WebhookProcessor) callWebhook(jobID int64, url string) error {
	log.WithField("job", jobID).Infof("Calling webhook %s", url)
	res, err := http.Post(url, "application/json", nil)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return nil
}
