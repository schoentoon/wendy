package workers

import (
	"database/sql"
	"net/http"
	"net/http/httptest"
	"sync"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"gitlab.com/schoentoon/wendy/pkg/database"
)

func TestWebhookProcessor(t *testing.T) {
	hook := test.NewGlobal()
	defer hook.Reset()
	called := false
	ts := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		called = true
		// TODO Later on when we actually specified a certain body we should inspect the body
	}))
	defer ts.Close()
	defer func() {
		if called == false {
			t.Error("Our httptest server was never actually called..")
		}
	}()

	db, cancel := database.TmpSqliteDB(t, true, func(db *sql.DB) error {
		_, err := db.Exec(`INSERT INTO deps(name) VALUES("docker.io/library/alpine:latest");`)
		if err != nil {
			return err
		}
		_, err = db.Exec(`INSERT INTO jobs(name) VALUES("test")`)
		if err != nil {
			return err
		}
		_, err = db.Exec(`INSERT INTO job_deps_link(job_id, dep_id) VALUES(1, 1)`)
		if err != nil {
			return err
		}
		_, err = db.Exec(`INSERT INTO webhooks(job_id, webhook) VALUES(1, ?)`, ts.URL)
		return err
	})
	defer cancel()

	processor := NewWebhookProcessor(db)
	var wg sync.WaitGroup
	wg.Add(1)
	go processor.Run(&wg)

	err := processor.BaseImageUpdated("docker.io/library/alpine:latest", 1)
	if err != nil {
		t.Errorf("Error from BaseImageUpdated: %s", err)
	}

	processor.Close()
	wg.Wait()

	assert.NotZero(t, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.LastEntry().Level)
	assert.Contains(t, hook.LastEntry().Message, "Calling webhook")
}
