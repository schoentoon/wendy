package images

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/docker/distribution/reference"
	log "github.com/sirupsen/logrus"
)

type DockerIO struct {
	client *http.Client
}

type tokenResp struct {
	Token string `json:"token"`
}

func (d *DockerIO) getToken(ref string) (string, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("https://auth.docker.io/token?service=registry.docker.io&scope=repository:%s:pull", ref), nil)
	if err != nil {
		return "", err
	}
	resp, err := d.client.Do(req)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	var out tokenResp
	err = json.NewDecoder(resp.Body).Decode(&out)
	return out.Token, err
}

func (d *DockerIO) Manifest(name reference.Named) (*Manifest, error) {
	// retrieve a pull token for the reference
	token, err := d.getToken(reference.Path(name))
	if err != nil {
		return nil, err
	}

	// retrieve the manifests for the reference and tag combination
	url := fmt.Sprintf("https://registry-1.docker.io/v2/%s/manifests/%s", reference.Path(name), Tag(name))
	log.WithField("image", name).Debugf("Fetching %s", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	// attach the auth token
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// decode json into manifest
	var out Manifest
	err = json.NewDecoder(resp.Body).Decode(&out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}
