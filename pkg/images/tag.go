package images

import (
	"github.com/docker/distribution/reference"
)

func Tag(named reference.Named) string {
	tagged, ok := named.(reference.Tagged)
	if ok {
		return tagged.Tag()
	}
	return "latest"
}
