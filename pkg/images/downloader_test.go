package images

import (
	"testing"

	"github.com/docker/distribution/reference"
	"github.com/stretchr/testify/assert"
)

func TestDownloadManager(t *testing.T) {
	manager := NewDownloadManager()

	name, err := reference.ParseNormalizedNamed("alpine:latest")
	if assert.Nil(t, err) {
		driver := manager.findDriver(name)
		assert.IsType(t, &DockerIO{}, driver)
	}

	name, err = reference.ParseNormalizedNamed("gcr.io/distroless/base:latest")
	if assert.Nil(t, err) {
		driver := manager.findDriver(name)
		assert.IsType(t, &Default{}, driver)
	}
}
