package images

import (
	"net/http"
	"sync"

	"github.com/docker/distribution/reference"
)

type Downloader interface {
	Manifest(name reference.Named) (*Manifest, error)
}

type DownloadManager struct {
	sync.RWMutex
	drivers           map[string]Downloader
	defaultDownloader Downloader
}

func NewDownloadManager() *DownloadManager {
	return &DownloadManager{
		drivers: map[string]Downloader{
			"docker.io": &DockerIO{client: http.DefaultClient},
		},
		defaultDownloader: &Default{},
	}
}

func (d *DownloadManager) findDriver(name reference.Named) Downloader {
	d.RLock()
	defer d.RUnlock()

	out, ok := d.drivers[reference.Domain(name)]
	if ok {
		return out
	}
	return d.defaultDownloader
}

func (d *DownloadManager) Manifest(name reference.Named) (*Manifest, error) {
	downloader := d.findDriver(name)

	m, err := downloader.Manifest(name)
	if err != nil {
		return nil, err
	}
	m.Named = name
	return m, nil
}
