package images

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/docker/distribution/reference"
	"github.com/stretchr/testify/assert"
)

func TestDockerIODriver(t *testing.T) {
	client := newTestClient(func(req *http.Request) *http.Response {
		switch req.URL.Hostname() {
		case "auth.docker.io":
			if assert.Equal(t, "/token", req.URL.Path) &&
				assert.Equal(t, "registry.docker.io", req.URL.Query().Get("service")) &&
				assert.Equal(t, "repository:library/alpine:pull", req.URL.Query().Get("scope")) {
				return &http.Response{
					StatusCode: 200,
					Header:     make(http.Header),
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"token":"TEST"}`)),
				}
			}
		case "registry-1.docker.io":
			if assert.Equal(t, "/v2/library/alpine/manifests/latest", req.URL.Path) &&
				assert.Equal(t, "Bearer TEST", req.Header.Get("Authorization")) {
				return &http.Response{
					StatusCode: 200,
					Header:     make(http.Header),
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"name":"library/alpine","tag":"latest"}`)),
				}
			}
		default:
			t.Errorf("Got an unexpected request: %s", req.URL.String())
		}
		return nil
	})

	driver := &DockerIO{client: client}

	name, err := reference.ParseNormalizedNamed("alpine:latest")
	if assert.Nil(t, err) {
		m, err := driver.Manifest(name)
		if assert.Nil(t, err) {
			assert.Equal(t, "library/alpine", m.Name)
		}
	}
}
