package images

import (
	"testing"

	"github.com/docker/distribution/reference"
	"github.com/stretchr/testify/assert"
)

func TestTag(t *testing.T) {
	parsed, err := reference.ParseNormalizedNamed("alpine:3.11")
	if assert.Nil(t, err) {
		assert.Equal(t, "3.11", Tag(parsed))
	}

	parsed, err = reference.ParseNormalizedNamed("gcr.io/distroless/base")
	if assert.Nil(t, err) {
		assert.Equal(t, "latest", Tag(parsed))
	}
}
