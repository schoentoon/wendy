package images

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/docker/distribution/reference"
	"github.com/stretchr/testify/assert"
)

func TestDefaultDriver(t *testing.T) {
	client := newTestClient(func(req *http.Request) *http.Response {
		switch req.URL.Hostname() {
		case "gcr.io":
			if assert.Equal(t, "/v2/distroless/base/manifests/latest", req.URL.Path) {
				return &http.Response{
					StatusCode: 200,
					Header:     make(http.Header),
					Body:       ioutil.NopCloser(bytes.NewBufferString(`{"name":"distroless/base","tag":"latest"}`)),
				}
			}
		default:
			t.Errorf("Got an unexpected request: %s", req.URL.String())
		}
		return nil
	})

	driver := &Default{client: client}

	name, err := reference.ParseNormalizedNamed("gcr.io/distroless/base")
	if assert.Nil(t, err) {
		m, err := driver.Manifest(name)
		if assert.Nil(t, err) {
			assert.Equal(t, "distroless/base", m.Name)
		}
	}
}
