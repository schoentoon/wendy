package images

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/docker/distribution/reference"
	log "github.com/sirupsen/logrus"
)

type Default struct {
	client *http.Client
}

func (d *Default) Manifest(name reference.Named) (*Manifest, error) {
	// retrieve the manifests for the reference and tag combination
	url := fmt.Sprintf("https://%s/v2/%s/manifests/%s", reference.Domain(name), reference.Path(name), Tag(name))
	log.WithField("image", name).Debugf("Fetching %s", url)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	resp, err := d.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// decode json into manifest
	var out Manifest
	err = json.NewDecoder(resp.Body).Decode(&out)
	if err != nil {
		return nil, err
	}
	return &out, nil
}
