package images

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestManifestChanged(t *testing.T) {
	a := &Manifest{
		Layers: []Layer{
			Layer{Sum: "a"},
		},
	}

	b := &Manifest{
		Layers: []Layer{
			Layer{Sum: "a"},
			Layer{Sum: "b"},
		},
	}

	c := &Manifest{
		Layers: []Layer{
			Layer{Sum: "b"},
		},
	}

	assert.False(t, a.Changed(a))
	assert.True(t, a.Changed(b))
	assert.True(t, a.Changed(c))
}
