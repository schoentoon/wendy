package images

import (
	"github.com/docker/distribution/reference"
)

type Layer struct {
	Sum string `json:"blobSum"`
}

type Manifest struct {
	Name   string          `json:"name"`
	Named  reference.Named `json:"-"`
	Arch   string          `json:"architecture"`
	Layers []Layer         `json:"fsLayers"`
}

func (m *Manifest) Changed(cmp *Manifest) bool {
	// if we have a different amount of layers we changed
	if len(m.Layers) != len(cmp.Layers) {
		return true
	}

	// check if all the layers are exactly the same or not
	for i, layer := range m.Layers {
		if cmp.Layers[i].Sum != layer.Sum {
			return true
		}
	}

	// if everything seems the same we know that we didn't change
	return false
}
