package handlers

import (
	"net/http"

	log "github.com/sirupsen/logrus"
)

func ErrorHandler(w http.ResponseWriter) {
	err := recover()
	if err == nil {
		return
	}
	entry, ok := err.(*log.Entry)
	if ok {
		http.Error(w, entry.Message, http.StatusInternalServerError)
	} else {
		panic(err)
	}
}
