package handlers

import (
	"encoding/json"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/schoentoon/wendy/pkg/database"
)

func TestAddDockerfileHandler(t *testing.T) {
	const inJSON = `{
		"name":"test",
		"dockerfile":"FROM alpine:latest",
		"webhook":"https://example.com/webhook"
	}`

	expectedBody := AddDockerfileResponse{
		Job:        "test",
		BaseImages: []string{"docker.io/library/alpine:latest"},
	}

	db, cancel := database.TmpSqliteDB(t, true, nil)
	defer cancel()

	req := httptest.NewRequest("POST", "http://localhost:55555/api/new/dockerfile", strings.NewReader(inJSON))
	w := httptest.NewRecorder()

	handler := &AddDockerfile{DB: db}
	handler.ServeHTTP(w, req)

	resp := w.Result()
	var parsed AddDockerfileResponse
	err := json.NewDecoder(resp.Body).Decode(&parsed)
	if err != nil {
		t.Error(err)
	}

	assert.Equal(t, 201, resp.StatusCode)
	assert.Equal(t, expectedBody, parsed)

	var count int
	row := db.QueryRow(`SELECT count(*) FROM deps WHERE name = "docker.io/library/alpine:latest"`)
	if assert.Nil(t, row.Scan(&count)) {
		assert.Equal(t, 1, count)
	}

	row = db.QueryRow(`SELECT count(*) FROM jobs WHERE name = "test"`)
	if assert.Nil(t, row.Scan(&count)) {
		assert.Equal(t, 1, count)
	}

	row = db.QueryRow(`SELECT count(*) FROM webhooks WHERE webhook = "https://example.com/webhook"`)
	if assert.Nil(t, row.Scan(&count)) {
		assert.Equal(t, 1, count)
	}
}
