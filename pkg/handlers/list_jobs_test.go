package handlers

import (
	"database/sql"
	"encoding/json"
	"io"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/schoentoon/wendy/pkg/database"
)

func TestListJobsHandler(t *testing.T) {
	expectedBody := []ListJobsEntry{
		ListJobsEntry{
			Name: "test",
		},
		ListJobsEntry{
			Name: "test2",
		},
	}

	db, cancel := database.TmpSqliteDB(t, true, func(db *sql.DB) error {
		_, err := db.Exec(`INSERT INTO jobs(name) VALUES("test")`)
		if err != nil {
			return err
		}
		_, err = db.Exec(`INSERT INTO jobs(name) VALUES("test2")`)
		return err
	})
	defer cancel()

	req := httptest.NewRequest("GET", "http://localhost:55555/api/list/jobs", nil)
	w := httptest.NewRecorder()

	handler := &ListJobs{DB: db}
	handler.ServeHTTP(w, req)

	resp := w.Result()
	assert.Equal(t, 200, resp.StatusCode)

	decoder := json.NewDecoder(resp.Body)
	for i := 0; ; i++ {
		var entry ListJobsEntry
		err := decoder.Decode(&entry)
		if err == io.EOF {
			break
		} else if err != nil {
			t.Error(err)
		}
		assert.Equal(t, expectedBody[i], entry)
	}
}
