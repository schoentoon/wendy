package handlers

import (
	"database/sql"
	"encoding/json"
	"io/ioutil"
	"net/http"

	log "github.com/sirupsen/logrus"

	"gitlab.com/schoentoon/wendy/pkg/parsers"
)

type AddDockerfile struct {
	DB *sql.DB
}

type AddDockerfileRequest struct {
	Name       string `json:"name"`
	Dockerfile string `json:"dockerfile"`
	Webhook    string `json:"webhook"`
}

type AddDockerfileResponse struct {
	Job        string   `json:"job"`
	BaseImages []string `json:"base_images"`
}

func (h *AddDockerfile) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer ErrorHandler(w)

	content, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Panicf("ioutil.ReadAll: %s", err)
	}

	clog := log.WithField("content", string(content))

	var in AddDockerfileRequest
	err = json.Unmarshal(content, &in)
	if err != nil {
		log.Panicf("json.Decode: %s", err)
	}

	if in.Name == "" || in.Dockerfile == "" {
		clog.Panicf("Missing arguments")
	}

	df, err := parsers.ParseDockerfile(in.Dockerfile)
	if err != nil {
		clog.Panicf("ParseDockerfile: %s", err)
	}

	tx, err := h.DB.Begin()
	if err != nil {
		clog.Panicf("Begin transaction: %s", err)
	}
	defer func() {
		err := tx.Rollback()
		if err != nil && err != sql.ErrTxDone {
			clog.Errorf("An error occurred while rolling back a transaction: %s", err)
		}
	}()

	out := &AddDockerfileResponse{
		Job:        in.Name,
		BaseImages: []string{},
	}

	var job_id int64
	job_res, err := tx.Exec(`INSERT INTO jobs(name) VALUES(?)`, in.Name)
	if err != nil {
		clog.Panicf("Inserting job: %s", err)
	}
	job_id, err = job_res.LastInsertId()
	if err != nil {
		clog.Panicf("LastInsertId(): %s", err)
	}

	for _, base := range df.BaseImages {
		out.BaseImages = append(out.BaseImages, base)

		var dep_id int64
		row := tx.QueryRow(`SELECT id FROM deps WHERE name = ?`, base)
		err = row.Scan(&dep_id)
		if err == sql.ErrNoRows {
			res, err := tx.Exec(`INSERT INTO deps(name) VALUES(?)`, base)
			if err != nil {
				clog.Panicf("Inserting dependency: %s", err)
			}
			dep_id, err = res.LastInsertId()
			if err != nil {
				clog.Panicf("LastInsertId(): %s", err)
			}
		}

		_, err = tx.Exec(`INSERT INTO job_deps_link(job_id, dep_id) VALUES(?, ?)`, job_id, dep_id)
		if err != nil {
			clog.Panicf("Inserting job_deps link: %s", err)
		}
	}

	_, err = tx.Exec(`INSERT INTO webhooks(job_id, webhook) VALUES(?, ?)`, job_id, in.Webhook)
	if err != nil {
		clog.Panicf("Inserting webhook: %s", err)
	}

	err = tx.Commit()
	if err != nil {
		clog.Panicf("Commit: %s", err)
	} else {
		w.WriteHeader(http.StatusCreated)
		err = json.NewEncoder(w).Encode(out)
		if err != nil {
			clog.Panicf("json Encode: %s", err)
		}
	}
}
