package handlers

import (
	"database/sql"
	"encoding/json"
	"net/http"

	log "github.com/sirupsen/logrus"
)

type ListJobs struct {
	DB *sql.DB
}

type ListJobsRequest struct {
}

type ListJobsEntry struct {
	Name string `json:"name"`
}

func (j *ListJobs) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	rows, err := j.DB.Query(`SELECT name FROM jobs`)
	if err != nil {
		log.Panicf("Query: %s", err)
	}
	for rows.Next() {
		var name string
		err = rows.Scan(&name)
		if err != nil {
			log.Panicf("Scan: %s", err)
		}
		out := &ListJobsEntry{
			Name: string(name),
		}
		err = json.NewEncoder(w).Encode(out)
		if err != nil {
			log.Panicf("Encode: %s", err)
		}
	}
}
