package database

import (
	"database/sql"
	"testing"
)

func TmpSqliteDB(t *testing.T, wendy bool, fill func(db *sql.DB) error) (*sql.DB, func()) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		t.Fatal(err)
	}
	cancel := func() {
		err := db.Close()
		if err != nil {
			t.Fatal(err)
		}
	}
	if wendy {
		migrator := NewWendyMigrator()
		err = migrator.Migrate(db)
		if err != nil {
			defer cancel()
			t.Fatal(err)
		}
	}
	if fill != nil {
		err = fill(db)
		if err != nil {
			defer cancel()
			t.Fatal(err)
		}
	}

	return db, cancel
}
