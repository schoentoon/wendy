package database

import (
	"database/sql"
	"testing"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/assert"
)

func TestMigrator(t *testing.T) {
	db, err := sql.Open("sqlite3", "file:test.db?mode=memory")
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	// create a migrator for version 1
	migrator := New(
		&Migration{
			Name: "Test 1",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`create table test1 (id integer not null primary key, name text);`)
				return err
			},
		},
	)

	// it shouldn't error
	assert.Nil(t, migrator.Migrate(db))

	// now let's see if table test1 is created
	row := db.QueryRow(`SELECT count(*) FROM test1`)
	var count int
	assert.Nil(t, row.Scan(&count))

	// and table test2 shouldn't exist yet
	row = db.QueryRow(`SELECT count(*) FROM test2`)
	assert.Error(t, row.Scan(&count))

	// running the same migrator again shouldn't cause issues
	assert.Nil(t, migrator.Migrate(db))

	// create a migrator for version 2, containing the first table and a second table as well
	migrator2 := New(
		&Migration{
			Name: "Test 1",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`create table test1 (id integer not null primary key, name text);`)
				return err
			},
		},
		&Migration{
			Name: "Test 2",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`create table test2 (id integer not null primary key, name text);`)
				return err
			},
		},
	)

	// running this migrator shouldn't cause any issues
	assert.Nil(t, migrator2.Migrate(db))

	row = db.QueryRow(`SELECT count(*) FROM test1`)
	assert.Nil(t, row.Scan(&count))

	// table test2 should exist now
	row = db.QueryRow(`SELECT count(*) FROM test2`)
	assert.Nil(t, row.Scan(&count))
}
