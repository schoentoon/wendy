package database

import (
	"database/sql"

	_ "github.com/mattn/go-sqlite3"
)

func NewWendyMigrator() *Migrator {
	return New(
		&Migration{
			Name: "Jobs table",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`CREATE TABLE jobs
					(
						id INTEGER NOT NULL PRIMARY KEY,
						name TEXT NOT NULL,
						CONSTRAINT name_unique UNIQUE(name)
					);
				`)
				return err
			},
		},
		&Migration{
			Name: "Dependency table",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`CREATE TABLE deps
					(
						id INTEGER NOT NULL PRIMARY KEY,
						name TEXT NOT NULL,
						manifest TEXT,
						CONSTRAINT name_unique UNIQUE(name)
					);
				`)
				return err
			},
		},
		&Migration{
			Name: "Dependency job links",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`CREATE TABLE job_deps_link
					(
						id INTEGER NOT NULL PRIMARY KEY,
						job_id INTEGER NOT NULL,
						dep_id INTEGER NOT NULL,
						FOREIGN KEY(job_id) REFERENCES jobs(id),
						FOREIGN KEY(dep_id) REFERENCES deps(id)
					);
				`)
				return err
			},
		},
		&Migration{
			Name: "Webhooks",
			Func: func(tx *sql.Tx) error {
				_, err := tx.Exec(`CREATE TABLE webhooks
					(
						id INTEGER NOT NULL PRIMARY KEY,
						job_id INTEGER NOT NULL,
						webhook TEXT NOT NULL,
						FOREIGN KEY(job_id) REFERENCES jobs(id)
					);
				`)
				return err
			},
		},
	)
}
