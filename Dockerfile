FROM golang as builder

WORKDIR /app

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o /bin/wendyd ./cmd/wendyd/...

FROM gcr.io/distroless/base

COPY --from=builder /bin/wendyd /bin/wendyd

CMD [ "/bin/wendyd" ]

EXPOSE 55555/tcp