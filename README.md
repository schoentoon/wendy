Wendy
=====
**This is still actively being worked on, expect breaking changes once in a while**

Wendy is a build manager for your docker images.
It'll monitor the base images that your Dockerfiles depend on and rebuild them whenever it detects changes.
For now it consists of a daemon (wendyd) and a simple command line interface (wendycli) to talk to the API with (web ui might follow in the future).
The actual building of images is outside of the scope of this tool, for now it can simply send out webhooks to your CI of choice.
For storage a sqlite3 database is used, this makes it rather quickly to get started with.

# Getting Started

After installing, copy `config.example.yml` and change it to your needs.
Afterwards simply launch `wendyd`, in case you didn't name your config file `config.yml` specify the `-config` flag with a path to your config file.
Now simply point wendycli to your wendyd instance, like `wendycli http://localhost:55555`.